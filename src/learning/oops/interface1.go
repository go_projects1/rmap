package oops

type Stringer interface {
	ToString() string
}

type Employee struct {
	Firstname string
	Lastname  string
}

func (e Employee) ToString() string {
	return e.Firstname + " " + e.Lastname
}

type Car struct {
	Model string
	Owner Employee
}

func (c Car) ToString() string {
	e := c.Owner
	return "It's " + e.ToString() + "'s car. It's model is " + c.Model
}
