package db

import "github.com/jmoiron/sqlx"

// SQLStorage ...
type SQLStorage struct {
	db *sqlx.DB
}

func (s SQLStorage) Tx() (*sqlx.Tx, error) {
	return s.db.Beginx()
}
