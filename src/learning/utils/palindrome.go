package utils

import "fmt"

func IsPalindrome(s string)  {
	if isPalindrome(s) {
		fmt.Printf("%s is palindrome data",s)
	}else {
		fmt.Printf("%s is not palindrome data",s)
	}
	fmt.Println()
}

func isPalindrome(s string) bool {
	var l = len(s)
	var i, j, k int
	if l%2 != 0 { //size is odd number
		i = int(l/2) + 1
	} else {
		i = l / 2
	}
	k = l - 1
	for j = 0; j < i; j++ {
		if s[j] != s[k] {
			return false
		}
		k -= 1
	}
	return true
}