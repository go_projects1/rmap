package utils

import "fmt"

func ShowFibonacci(n int){
	for i := 0; i <n; i++ {
		fmt.Print(getNthFibonacci(i)," ")
	}
	fmt.Println()
}

func getNthFibonacci(i int) int  {
	if i<=1 {
		return 1
	}else {
		return getNthFibonacci(i-1)+getNthFibonacci(i-2)
	}
}