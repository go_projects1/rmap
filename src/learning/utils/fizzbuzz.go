package utils

import "fmt"

func ShowNFizzBuzz(n int) {
	var i int
	for i = 1; i <= n; i++ {
		if i%3 == 0 && i%5 == 0 {
			fmt.Print("fizz buzz")
		} else if i%3 == 0 {
			fmt.Print("fizz")
		} else if i%5 == 0 {
			fmt.Print("buzz")
		} else {
			fmt.Print(i)
		}
		fmt.Print(" ")
	}
	fmt.Println()
}
