package main

import (
	"fmt"
	"learning/oops"
	_ "learning/rest"
	_ "learning/storage"
	"learning/utils"
)

func main() {

	//show n fibonacci
	utils.ShowFibonacci(10)

	//is palindrome
	utils.IsPalindrome("1233213")

	//show fizz buzz numbers
	utils.ShowNFizzBuzz(16)

	//interface1
	e := oops.Employee{"Maqsadali", "Husanov"}
	car := oops.Car{"BMW", e}

	fmt.Println(e.ToString())
	fmt.Println(car.ToString())

}
